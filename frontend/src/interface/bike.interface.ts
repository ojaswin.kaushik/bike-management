export interface Ibike{
    bikeId:number,
    bikeModel:string,
    bikeColor:string,
    bikeLocation:string,
    rating:number
}