export default interface Iuser{
    userId:number,
    userName:string,
    userType?: boolean,
    userEmail: string,
    password?:string
}