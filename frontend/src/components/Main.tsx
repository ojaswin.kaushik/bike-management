import React from "react";
import { BikeList } from "./BikeList";
import Grid from "@mui/material/Grid";
import { Filter } from "./Filter";

export default function Main() {
    return <>

        <div className={ "BikeContainer" }>
            <h1>Bike <span style={ { color: "black" } }>Management</span></h1>

        </div>
        <Grid container alignItems={ "center" } justifyContent={ "center" }>
            <Filter/>
        </Grid>
        <Grid container alignItems={ "center" } justifyContent={ "center" } style={ { margin: "5%" } }>

            <BikeList/>

        </Grid>

    </>
}