import React from "react";
import { Ibike } from "../interface/bike.interface";
import Grid from "@mui/material/Grid";
import { MenuIcon } from "./MenuIcon";

export function Bike({ bikeId, bikeColor, bikeLocation, bikeModel, rating }: Ibike) {

    const circle = {
        width: "40px",
        height: "40px",
        backgroundColor: "#ba6809",
        display: "flex",
        justifyContent: "center",
        borderRadius: "50% 50%",
        color: "white"

    }

    return <div style={ { backgroundColor: "#ba6809", height: "22em", width: "25em", margin: "5%", display: "flex" } }>
        <div className={ "bikeCard" }>
            <Grid container direction={ "row" } alignItems={ "center" } justifyContent={ "space-around" }>
                <h2 style={ circle }>{ bikeId }</h2>
                <MenuIcon/>
            </Grid>
            <section>
                <h4>

                    <span
                        style={ { color: "#ba6809", fontSize: "25px" } }>M</span>odel &nbsp; : &nbsp;
                    <span>{ bikeModel }</span>
                </h4>
                <h4>
                    <span
                        style={ { color: "#ba6809", fontSize: "25px" } }>L</span>
                    <span>ocation &nbsp; : &nbsp; { bikeLocation }</span>
                </h4>
                <h4>
                    <span
                        style={ { color: "#ba6809", fontSize: "25px" } }>C</span>
                    <span>olor &nbsp; : &nbsp; { bikeColor }</span>
                </h4>
                <h4>
                    <span
                        style={ { color: "#ba6809", fontSize: "25px" } }>R</span>
                    <span>ating &nbsp; : &nbsp; { rating }</span>
                </h4>
            </section>
        </div>
        <div className={ "crud" }>
            Bike
        </div>
    </div>
}