import React from "react";
import {Link} from "react-router-dom";


export default function Headers(){
    return <div>
        <nav className="navbar">
           <ul className="navList">
               <li>
                   <Link to={"/"} className="Link">Home</Link>
               </li>
               <li>
                   <Link to={"/login"} className="Link">Login</Link>
               </li>
               <li>
                    <Link to={"/signin"} className="Link">Signin</Link>
                </li>
               <li>
                   <Link to={"/reservation"} className="Link">Reservation</Link>
               </li>
               <li>
                   <Link to={"/user"} className="Link">User</Link>
               </li>

            </ul>
        </nav>
    </div>
}