import React, { useState } from "react";
import { Button, IconButton } from "@mui/material";
import MoreVertSharpIcon from "@mui/icons-material/MoreVertSharp";
import EditSharpIcon from '@mui/icons-material/EditSharp';
import DeleteSharpIcon from '@mui/icons-material/DeleteSharp';

export  function MenuIcon(){
    const [flag,setFlag]=useState(false)

    return <>
        <IconButton style={{color:"black"}} onClick={()=>{setFlag(!flag)}} >
            <MoreVertSharpIcon/>
        </IconButton>
        {flag?
            <IconButton
                style={{transform :"translate(100px,-80px)",position:"absolute",color:"black"}}>
            <EditSharpIcon/>
        </IconButton>:null}
        {flag?
            <Button
                style={{
                    transform :"translate(180px,-80px)",position:"absolute",
                    color:"#ba6809",fontSize:"15px",fontFamily:"Aboreto",
                    fontWeight:"900",border:"2px solid brown"
            }}>
                Book Now
            </Button>:null
        }
        {flag?
            <IconButton
                style={{transform :"translate(180px,0px)",position:"absolute",color:"black"}}>
            <DeleteSharpIcon/>
        </IconButton>:null}
    </>
}