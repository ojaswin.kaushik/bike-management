import React, { useEffect, useState } from "react";
import { bikeData } from "../util";
import { Ibike } from "../interface/bike.interface";
import Grid from "@mui/material/Grid";
import { Bike } from "./Bike";

export function BikeList() {
    const [ bikes, setBikes ] = useState<Ibike[]>( [] )
    useEffect( () => {
        setBikes( bikeData )
    }, [] )
    if (bikes.length === 0) return <h1>No bikes are present</h1>
    return (
        <>
            {
                bikes.map( (bike) => {

                    return (
                        <Grid key={ bike.bikeId }
                              item xs={ 6 }
                              alignItems={ "center" }
                              justifyContent={ "center" }>

                            <Bike { ...bike }/>

                        </Grid>
                    )

                } )
            }
        </>
    )
}