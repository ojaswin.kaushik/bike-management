import React from "react";
import Grid from "@mui/material/Grid";
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import { TextField } from "@mui/material";
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs, { Dayjs } from "dayjs";

export function Filter() {

    const [ fromDate, setFromDate ] = React.useState <Dayjs | null>( null );
    const [ toDate, setToDate ] = React.useState <Dayjs | null>( null );

    return (
        <Grid container direction={ "column" } alignItems={ "center" } justifyContent={ "center" }>
            <Grid container direction={ "row" } alignItems={ "center" } justifyContent={ "center" } spacing={ 3 }>

                <Grid item>
                    <TextField id="standard-Model" label="Model" variant="standard" className="textfield"
                    />
                </Grid>
                <Grid item>
                    <TextField id="standard-Color" label="Color" variant="standard"/>
                </Grid>
                <Grid item>
                    <TextField id="standard-Rating" label="Rating" variant="standard" type="number"
                               style={ { width: "150px" } }
                               InputProps={ {
                                   inputProps: {
                                       max: 5, min: 1
                                   }
                               } }/>
                </Grid>
                <Grid item>
                    <TextField id="standard-Location" label="Location" variant="standard"/>
                </Grid>

            </Grid>
            <Grid container style={ { margin: "2%" } } alignItems={ "center" } justifyContent={ "center" }>
                <LocalizationProvider dateAdapter={ AdapterDayjs }>
                    <h3>From&nbsp;</h3>
                    <DatePicker
                        disableFuture
                        label="From"
                        openTo="year"
                        views={ [ 'year', 'month', 'day' ] }
                        value={ fromDate }
                        onChange={ (newValue) => {
                            setFromDate( newValue );
                        } }
                        renderInput={ (params) => <TextField { ...params } /> }
                    />
                </LocalizationProvider>

                <LocalizationProvider dateAdapter={ AdapterDayjs }>
                    <h3>&nbsp;TO &nbsp;</h3>
                    <DatePicker
                        disableFuture
                        label="To"
                        openTo="year"
                        views={ [ 'year', 'month', 'day' ] }
                        value={ toDate }
                        onChange={ (newValue) => {
                            setToDate( newValue );
                        } }
                        renderInput={ (params) => <TextField { ...params } /> }
                    />
                </LocalizationProvider>
            </Grid>
            <h2>
                <ArrowUpwardIcon
                    style={ { color: "chocolate", fontSize: "30px" } }/>
                &nbsp;Filter Results Accordingly
            </h2>
        </Grid>


    )
}