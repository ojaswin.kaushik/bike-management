import Joi from "joi";

export const userSignInSchema=Joi.object().keys({
    username:Joi.string().required(),
    email:Joi.string().email({ tlds: { allow: false}}).required(),
    password:Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
})

export const userLoginSchema=Joi.object().keys({
    email:Joi.string().email({ tlds: { allow: false}}).required(),
    password:Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
})

export const bikeData:any=[]

for(let i=0;i<20;i++){
    let newBike={
        bikeId:i,
        bikeModel:`Model ${i}`,
        bikeColor: `Color ${i}`,
        bikeLocation:`Location ${i}`,
        rating:Math.ceil(Math.random()*5)
    }

    bikeData.push(newBike)
}

export const userData:any=[]

for(let i=0;i<5;i++){
    let newUser={
        userId:i,
        userName:`UserName ${i}`,
        userType:i%2===0,
        userEmail:`UserName${i}.gmail.com`
    }
    userData.push(newUser)
}
