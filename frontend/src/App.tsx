import React from 'react';
import './App.css';
import {BrowserRouter,Route,Routes} from "react-router-dom";
import Sign from "./Pages/Sign";
import Login from "./Pages/Login";
import Main from "./components/Main";
import Header from "./components/Header";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Reservation } from "./Pages/Reservation";
import { User } from "./Pages/User";

function App() {
  return (
    <div >

        <BrowserRouter>
            <Header/>
            <ToastContainer/>
            <Routes>
                <Route path="/" element={<Main/>} />
                <Route path="login" element={<Login/>} />
                <Route path="signin" element={<Sign/>} />
                <Route path="reservation" element={<Reservation/>} />
                <Route path="user" element={<User/>} />
            </Routes>
        </BrowserRouter>

    </div>
  );
}

export default App;
