import React, { useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import { userData as Data } from "../util";
import Iuser from "../interface/user.interface";
import { IconButton, Paper } from "@mui/material";
import AddCircleIcon from '@mui/icons-material/AddCircle';

export function User() {
    const [ userData, setUserData ] = useState<Iuser[]>( [] )
    useEffect( () => {
        setUserData( Data )
    } )
    const userCard = {
        width: "50vw",
        margin: "1%",
        padding: "1%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column" as 'column',
        backgroundColor: "#f3d7a8",
        borderLeft: "3px solid #756225",


    }
    const addUserStyle = {
        position: "fixed" as "fixed",
        right: "7%",
        color: "#b08656",
        bottom: "10%"

    }
    return (
        <Grid container alignItems={ "center" } justifyContent={ "center" } direction={ "column" }>
            <Grid container alignItems={ "center" } justifyContent={ "space-around" }>
                <h1>User Details</h1>
                <IconButton style={ addUserStyle }>
                    <AddCircleIcon style={ { fontSize: "50px" } }/>
                    Add User
                </IconButton>

            </Grid>

            {
                userData.map( (user) => {
                    return (
                        <Grid key={ user.userId } item xs={ 6 }>
                            <Paper style={ userCard }>
                                <h2 style={ {
                                    backgroundColor: "#756225"
                                    , width: "102%",
                                    padding: '0',
                                    margin: '0',
                                    textAlign: "center",
                                    color: "white"
                                } }>{ user.userId }</h2>
                                <h3>UserName :- { user.userName }</h3>
                                <h3>UserEmail :- { user.userEmail }</h3>
                                <h4>{ user.userType ? "Manager" : "Regular" }</h4>
                            </Paper>
                        </Grid>
                    )
                } )
            }

        </Grid>

    )
}