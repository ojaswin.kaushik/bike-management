import {
    Button,
    TextField
} from '@mui/material'

import Grid from '@mui/material/Grid';
import React, { useState } from 'react'
import { toast } from "react-toastify";
import { userLoginSchema } from "../util";

export default function Login() {
    const [ email, setEmail ] = useState( "" )
    const [password,setPassword]=useState("")
    const handleSubmit =   async () => {
        console.log(email,password)
        try{
            const {error,value}=await userLoginSchema.validate({email:email,password:password})
            if (error!==undefined){
                toast.error(error.message)
            }
            else{
                toast.success("Successfully Logged In !", {
                    position: toast.POSITION.TOP_RIGHT
                });
                console.log(error,value,'ee')
            }
        }catch (e){
            console.log("error",e)
            console.log('wow')
        }



    }
    const text={
       style:{ fontSize:18}
    }


    return (


        <Grid
            style={ { marginTop: "5%",paddingBottom:"10%" } }
            container
            alignItems={ "center" }
            justifyContent={ "center" }
            direction={ "column" }
            spacing={ 5 }>

            <h1>Login</h1>
            <Grid item >
                <TextField
                    onInput={e=>setEmail((e.target as HTMLInputElement).value)}
                    InputLabelProps={text}
                    InputProps={text}
                    autoFocus
                    margin="dense"
                    id="email"
                    label="Email Address"
                    type="email"
                    fullWidth
                    variant="standard"/>
            </Grid>
            <Grid item>
                <TextField
                    onInput={e=>setPassword((e.target as HTMLInputElement).value)}
                    InputLabelProps={text}
                    InputProps={text}
                    autoFocus
                    margin="dense"
                    id="password"
                    label="Password"
                    type="password"
                    variant="standard"/>
            </Grid>
            <Button
                onClick={handleSubmit}
                style={{margin:'40px',fontSize:"14px"}}
                type={"submit"}
                variant={"contained"}>
                Login</Button>

        </Grid>

    )
}