import React, { useState } from 'react'
import Grid from "@mui/material/Grid";
import { Button, TextField } from "@mui/material";
import { userSignInSchema } from "../util";
import { toast } from "react-toastify";
export default function Sign() {
    const [username,setUsername]=useState("")
    const [email,setEmail]=useState("");
    const [password,setPassword]=useState("");
    const text={
        style:{
            fontSize:18
        }
    }
    const handleSubmit=()=>{
        const {error,value}=userSignInSchema.validate({username,email,password})
        if (error!==undefined){
            console.log(error.message,value)
            toast.error(error.message)
            return
        }
        toast.success("Signin successful")
    }
    return (

        <Grid
            style={ { marginTop: "3%",paddingBottom:"10%" } }
            container
            alignItems={ "center" }
            justifyContent={ "center" }
            direction={ "column" }
            spacing={ 2}>

            <h1>SignIn</h1>
            <Grid item >
                <TextField
                    onInput={e=>setUsername((e.target as HTMLInputElement).value)}
                    InputLabelProps={text}
                    InputProps={text}
                    autoFocus
                    margin="dense"
                    id="username"
                    label="Username"
                    type="text"
                    fullWidth
                    variant="standard"/>
            </Grid>
            <Grid item >
                <TextField
                    onInput={e=>setEmail((e.target as HTMLInputElement).value)}
                    InputLabelProps={text}
                    InputProps={text}
                    autoFocus
                    margin="dense"
                    id="email"
                    label="Email Address"
                    type="email"
                    fullWidth
                    variant="standard"/>
            </Grid>
            <Grid item>
                <TextField
                    onInput={e=>setPassword((e.target as HTMLInputElement).value)}
                    InputLabelProps={text}
                    InputProps={text}
                    autoFocus
                    margin="dense"
                    id="password"
                    label="Password"
                    type="password"
                    variant="standard"/>
            </Grid>
            <Button
                onClick={handleSubmit}
                style={{margin:'40px',fontSize:"14px"}}
                type={"submit"}
                variant={"contained"}>
                Sign In</Button>

        </Grid>
    )
}