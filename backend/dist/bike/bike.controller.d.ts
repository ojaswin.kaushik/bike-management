import { BikeService } from './bike.service';
export declare class BikeController {
    private bikeService;
    constructor(bikeService: BikeService);
    findAll(): Promise<import("../entities/bike.entity").Bike[]>;
    findById(id: string): Promise<import("../entities/bike.entity").Bike>;
    createBike(body: any): Promise<string>;
    deleteBike(id: string): Promise<string>;
}
