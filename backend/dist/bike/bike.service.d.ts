import { Bike } from '../entities/bike.entity';
import { Repository } from 'typeorm';
export declare class BikeService {
    private bikeRepository;
    constructor(bikeRepository: Repository<Bike>);
    findAll(): Promise<Bike[]>;
    findById(id: number): Promise<Bike>;
    createBike(bikeDetails: any): Promise<string>;
    deleteBike(id: number): Promise<string>;
}
