"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BikeService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const bike_entity_1 = require("../entities/bike.entity");
const typeorm_2 = require("typeorm");
let BikeService = class BikeService {
    constructor(bikeRepository) {
        this.bikeRepository = bikeRepository;
    }
    async findAll() {
        return this.bikeRepository.find();
    }
    async findById(id) {
        return this.bikeRepository.findOneBy({ id });
    }
    async createBike(bikeDetails) {
        const bike = await this.bikeRepository.findOneBy(Object.assign({}, bikeDetails));
        if (bike !== null) {
            throw new common_1.HttpException("Bike already exist", 400);
        }
        const newBike = await this.bikeRepository.create(bikeDetails);
        await this.bikeRepository.save(newBike);
        return "Bike Created" + " " + bikeDetails.model;
    }
    async deleteBike(id) {
        const bike = await this.findById(id);
        console.log(bike);
        if (bike === null) {
            throw new common_1.HttpException("Bike does not exist", 400);
        }
        await this.bikeRepository.remove(bike);
        return "Bike has been deleted :- " + bike[0].model;
    }
};
BikeService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(bike_entity_1.Bike)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], BikeService);
exports.BikeService = BikeService;
//# sourceMappingURL=bike.service.js.map