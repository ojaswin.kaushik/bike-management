import { User } from './user.entity';
export declare class Bike {
    id: number;
    model: string;
    location: string;
    color: string;
    rating: number;
    user: User;
}
