import { Bike } from "./bike.entity";
export declare class User {
    id: number;
    email: string;
    password: string;
    isManager: boolean;
    bike: Bike[];
}
