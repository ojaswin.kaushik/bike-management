import { SqliteConnectionOptions } from "typeorm/driver/sqlite/SqliteConnectionOptions";
declare const configDb: SqliteConnectionOptions;
export declare function comparePassword(plaintextPassword: any, hash: any): Promise<boolean>;
export declare const JwtSecret = "SECRET";
export default configDb;
