"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtSecret = exports.comparePassword = void 0;
const user_entity_1 = require("../entities/user.entity");
const bcrypt_1 = require("bcrypt");
const bike_entity_1 = require("../entities/bike.entity");
const configDb = {
    type: 'sqlite',
    database: 'bike.db',
    entities: [user_entity_1.User, bike_entity_1.Bike],
    synchronize: true,
};
async function comparePassword(plaintextPassword, hash) {
    console.log(plaintextPassword, hash);
    const result = await (0, bcrypt_1.compare)(plaintextPassword, hash);
    return result;
}
exports.comparePassword = comparePassword;
exports.JwtSecret = "SECRET";
exports.default = configDb;
//# sourceMappingURL=util.js.map