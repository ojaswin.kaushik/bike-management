import { CanActivate, ExecutionContext } from '@nestjs/common';
export default class ManagerGuard implements CanActivate {
    canActivate(context: ExecutionContext): Promise<boolean>;
}
