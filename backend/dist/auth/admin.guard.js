"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Jwt = require("jsonwebtoken");
const util_1 = require("../util/util");
const validate = (token) => {
    try {
        const decode = Jwt.verify(JSON.parse(JSON.stringify(token)), util_1.JwtSecret);
        return decode.isManager;
    }
    catch (e) {
        console.log(e);
    }
};
class ManagerGuard {
    async canActivate(context) {
        const request = context.switchToHttp().getRequest();
        if (request.headers.authorization === undefined)
            return false;
        const token = request.headers.authorization.split(' ')[1];
        const manager = validate(token);
        return manager;
    }
}
exports.default = ManagerGuard;
//# sourceMappingURL=admin.guard.js.map