import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import { Bike } from '../entities/bike.entity';
export declare class UserService {
    private usersRepository;
    private jwtService;
    private bikeRepository;
    constructor(usersRepository: Repository<User>, jwtService: JwtService, bikeRepository: Repository<Bike>);
    findAll(): Promise<User[]>;
    findById(id: number): Promise<User>;
    findByEmail(email: string): Promise<User>;
    signUp(userDetails: any): Promise<string>;
    login(userDetails: any): Promise<boolean | {
        access_token: string;
    }>;
    deleteUserById(id: number): Promise<string>;
    deleteAllUser(): Promise<void>;
    updateUser(id: number, userDetails: any): Promise<string>;
    addReserve(userId: number, bikeId: number): Promise<void>;
    deleteReserve(userId: number, bikeId: number): Promise<string>;
    getBikeReserve(userId: number): Promise<Bike[]>;
}
