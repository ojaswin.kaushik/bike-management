import { UserService } from "./user.service";
export declare class UserController {
    private UserService;
    constructor(UserService: UserService);
    findAll(): Promise<import("../entities/user.entity").User[]>;
    signUp(body: any): Promise<string>;
    login(body: any): Promise<boolean | {
        access_token: string;
    }>;
    updateUser(id: string, body: any): Promise<string>;
    deleteUserById(id: string): Promise<string>;
    deleteAll(): Promise<void>;
    addReserve(body: any): Promise<void>;
    deleteReserve(body: any): Promise<string>;
    getBikeReserve(id: string): Promise<import("../entities/bike.entity").Bike[]>;
}
