"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../entities/user.entity");
const bcrypt_1 = require("bcrypt");
const util_1 = require("../util/util");
const jwt_1 = require("@nestjs/jwt");
const bike_entity_1 = require("../entities/bike.entity");
let UserService = class UserService {
    constructor(usersRepository, jwtService, bikeRepository) {
        this.usersRepository = usersRepository;
        this.jwtService = jwtService;
        this.bikeRepository = bikeRepository;
    }
    findAll() {
        return this.usersRepository.find();
    }
    async findById(id) {
        return await this.usersRepository.findOneBy({ id });
    }
    async findByEmail(email) {
        return await this.usersRepository.findOneBy({ email });
    }
    async signUp(userDetails) {
        const user = await this.findByEmail(userDetails.email);
        if (user !== null) {
            throw new common_1.HttpException("Email already exists!!!", 400);
        }
        await (0, bcrypt_1.hash)(userDetails.password, 10, async (err, hash) => {
            if (err !== undefined) {
                throw new common_1.HttpException(err, 404);
            }
            const newUser = await this.usersRepository.create(Object.assign(Object.assign({}, userDetails), { isManager: userDetails.isManager == "true", password: hash }));
            await this.usersRepository.save(newUser);
        });
        return "User created " + userDetails.email;
    }
    async login(userDetails) {
        const user = await this.findByEmail(userDetails.email);
        console.log(user);
        if (user !== null) {
            const result = await (0, util_1.comparePassword)(userDetails.password, user.password);
            if (result) {
                console.log("user logged in");
                const { password } = user, payload = __rest(user, ["password"]);
                return {
                    access_token: this.jwtService.sign(payload)
                };
            }
            return result;
        }
        throw new common_1.HttpException("Invalid Credentials", 400);
    }
    async deleteUserById(id) {
        const user = await this.findById(id);
        console.log(user, "deleted");
        if (user !== null) {
            await this.usersRepository.remove(user);
            return "User deleted" + " " + user.email;
        }
        throw new common_1.HttpException("No such user exist", 400);
    }
    async deleteAllUser() {
        const users = await this.findAll();
        users.map((user) => {
            this.deleteUserById(user.id);
        });
    }
    async updateUser(id, userDetails) {
        const user = await this.findById(id);
        console.log(user, "in update");
        console.log(userDetails, id);
        if (user === undefined)
            throw new common_1.HttpException("No user exist", 400);
        const { password } = userDetails, rest = __rest(userDetails, ["password"]);
        const newUser = Object.assign(Object.assign({}, user), rest);
        await this.usersRepository.save(newUser);
        return "User Updated";
    }
    async addReserve(userId, bikeId) {
        const user = await this.usersRepository.findOne({
            where: { id: userId },
            relations: ['bike'],
        });
        const bike = await this.bikeRepository.findOneBy({ id: bikeId });
        if (user && bike) {
            const addBike = [...user.bike];
            addBike.push(bike);
            await this.usersRepository.save(Object.assign(Object.assign({}, user), { bike: [...addBike] }));
            console.log(user, bike);
        }
    }
    async deleteReserve(userId, bikeId) {
        const user = await this.usersRepository.findOne({
            where: { id: userId },
            relations: ['bike'],
        });
        const bike = await this.bikeRepository.findOneBy({ id: bikeId });
        if (user && bike) {
            const deleteBike = user.bike.filter((bike) => {
                return bike.id !== bikeId;
            });
            await this.usersRepository.save(Object.assign(Object.assign({}, user), { bike: [...deleteBike] }));
            console.log(user, deleteBike);
            return "Reservation completed";
        }
        throw new common_1.HttpException("User/Bike not found", 400);
    }
    async getBikeReserve(userId) {
        const user = await this.usersRepository.findOne({
            where: { id: userId },
            relations: ['bike'],
        });
        if (user) {
            return user.bike;
        }
        throw new common_1.HttpException("User not found", 400);
    }
};
UserService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(user_entity_1.User)),
    __param(2, (0, typeorm_1.InjectRepository)(bike_entity_1.Bike)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        jwt_1.JwtService,
        typeorm_2.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map