import { Body, Controller, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';
import ManagerGuard from '../auth/admin.guard';

import { BikeService } from './bike.service';

@Controller('bike')
export class BikeController {
    constructor(private bikeService :BikeService){}
    @Get()
    findAll(){
        return this.bikeService.findAll()
    }
    @Get('/:id')
    findById(@Param('id') id:string){
        return this.bikeService.findById(Number(id.slice(1)))
    }
    @Post('/create')
    @UseGuards(ManagerGuard)
    createBike(@Body() body:any)
    {
        return this.bikeService.createBike(body)
    }

    @Delete('/delete/:id')
    @UseGuards(ManagerGuard)
    deleteBike(@Param('id')id:string){
        console.log(Number(id.slice(1)));
        
        return this.bikeService.deleteBike(Number(id.slice(1)))
    }
}
