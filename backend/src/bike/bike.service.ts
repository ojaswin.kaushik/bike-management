import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Bike } from '../entities/bike.entity';
import { Repository } from 'typeorm';


@Injectable()
export class BikeService {

    constructor(@InjectRepository(Bike) private bikeRepository:Repository<Bike>){}

    async findAll()
    {
        return this.bikeRepository.find()
    }

    async findById(id:number){
        return this.bikeRepository.findOneBy({id})
    }
    
    async createBike(bikeDetails){
        const bike =await this.bikeRepository.findOneBy({...bikeDetails})
        if (bike!==null)
        {
            throw new HttpException("Bike already exist",400)
        }
        const newBike=await this.bikeRepository.create(bikeDetails)
        await this.bikeRepository.save(newBike)
        return "Bike Created"+" "+bikeDetails.model
        
    }
    
    async deleteBike(id:number){
        const bike =await this.findById(id)
        
        console.log(bike);
        
        if (bike===null)
        {
            throw new HttpException("Bike does not exist",400)
        }
        await this.bikeRepository.remove(bike)
        return "Bike has been deleted :- "+ bike[0].model
    }

    
}
