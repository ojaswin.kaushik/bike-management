import { Injectable } from '@nestjs/common';
import { comparePassword } from '../util/util';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {
  constructor(private userService: UserService ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    console.log(username);
    
    const user = await this.userService.findByEmail(username);
    console.log("user of auth",user);
    
    if (user && await comparePassword(pass,user.password)) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }
}