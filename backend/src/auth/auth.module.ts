import { Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';


@Module({
  imports:[UserModule,PassportModule],
  providers: [AuthService,LocalStrategy,JwtStrategy]
})
export class AuthModule {}
