import {
    CanActivate,
    ExecutionContext,
    ForbiddenException,
    UnauthorizedException,
  } from '@nestjs/common';
  import * as Jwt from 'jsonwebtoken';
  import { JwtSecret } from '../util/util';
  const validate = (token: string) => {
    try {
      
      const decode :any= Jwt.verify(JSON.parse(JSON.stringify(token)),JwtSecret);
        return decode.isManager
    } catch (e) {
      console.log(e);
    }
  };
  export default class ManagerGuard implements CanActivate {
      
    async canActivate(context: ExecutionContext): Promise<boolean> {
      
      
      
      const request = context.switchToHttp().getRequest();
      if (request.headers.authorization===undefined) return false
     
      
      const token=request.headers.authorization.split(' ')[1]
     
      const manager=validate(token)
      
      
      return manager
    //   console.log(request.headers.authorization);
      
    //   const token = request.headers.authorization.split(' ')[1];
      
      
    //   const isAdmin = validate(token);
    //   return isAdmin===1;
      //   if (user) {
  
      //     if (user.isAdmin) {
      //       return true;
      //     } else throw new ForbiddenException();
      //   } else throw new UnauthorizedException();
    }
  }
  