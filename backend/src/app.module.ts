import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { BikeModule } from './bike/bike.module';
import configDb from "./util/util";
@Module({
  imports: [UserModule,TypeOrmModule.forRoot(configDb),UserModule, AuthModule, BikeModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
