import { SqliteConnectionOptions } from "typeorm/driver/sqlite/SqliteConnectionOptions";
import {User} from "../entities/user.entity";
import {compare} from "bcrypt"
import { Bike } from "../entities/bike.entity";
const configDb:SqliteConnectionOptions={
    type:'sqlite',
    database:'bike.db',
    entities:[User,Bike],
    synchronize:true,
}

export async function comparePassword(plaintextPassword, hash) :Promise<boolean>{
    console.log(plaintextPassword,hash);
    
    const result:boolean = await compare(plaintextPassword, hash);
    return result;
}

export const JwtSecret="SECRET"
export default configDb