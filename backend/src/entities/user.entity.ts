import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Bike } from "./bike.entity";
@Entity()
export class User{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    email:string;

    @Column()
    password:string;

    @Column({default:false})
    isManager:boolean;

    @ManyToMany(()=>Bike,bike=>bike.user,{onDelete:'SET NULL'})
    @JoinTable({name:"Reserved"})
    bike:Bike[];
}