import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import {User} from './user.entity'
@Entity()
export class Bike{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    model:string;

    @Column()
    location:string;

    @Column()
    color:string;

    @Column()
    rating:number

    @ManyToMany(()=>User,user =>user.bike,{onDelete:"SET NULL"})
    user:User;
}