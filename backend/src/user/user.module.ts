import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "../entities/user.entity";
import { UserService } from "./user.service";
import { UserController } from "./user.controller";
import { JwtModule } from '@nestjs/jwt';
import { Bike } from '../entities/bike.entity';

@Module({
    imports:[TypeOrmModule.forFeature([User,Bike]),JwtModule.register({
        secret: "SECRET",
        signOptions: { expiresIn: '1d' },
      })
    ],
    providers:[UserService],
    controllers: [UserController],
    exports:[UserService]
})
export class UserModule {
}
