import { HttpException, Injectable, UseGuards } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity'
import {hash,compare} from "bcrypt"
import { comparePassword } from '../util/util';
import { AuthGuard } from '@nestjs/passport';
import { JwtService } from '@nestjs/jwt';
import { Bike } from '../entities/bike.entity';
@Injectable()
export class UserService {
    constructor(@InjectRepository(User) private usersRepository: Repository<User> ,
    private jwtService:JwtService,
    @InjectRepository(Bike) private bikeRepository: Repository<Bike> ) {
    }

    findAll(){
        return this.usersRepository.find()
    }

    async findById(id:number){
        return await this.usersRepository.findOneBy({id})
    }
    async findByEmail(email:string){
        
        
        return await this.usersRepository.findOneBy({email})
    }
    async signUp(userDetails:any){
        const user = await this.findByEmail(userDetails.email)
        
        
        if (user!==null){
            throw new HttpException("Email already exists!!!", 400);
        }
        await hash(userDetails.password,10,async(err,hash)=>{
            if (err!==undefined){
                throw new HttpException(err,404)
                
        }
            const newUser = await this.usersRepository.create({...userDetails,isManager:userDetails.isManager=="true",password:hash})
            await this.usersRepository.save(newUser)
        })
        
        
        
        return "User created "+userDetails.email
    }
    async login(userDetails:any) {
        const user:any=await this.findByEmail(userDetails.email)
        console.log(user);
        
        if (user!==null) {
            const result=await comparePassword(userDetails.password,user.password)
            if (result)
                {
                    console.log("user logged in");
                    const {password,...payload} =user
                    return {
                        access_token:this.jwtService.sign(payload)
                    }
                }

            return result
        }
        throw new HttpException("Invalid Credentials",400)
        
    }
    async deleteUserById(id:number){
        const user =await this.findById(id)
        console.log(user,"deleted");
        
        if (user!==null){
            await this.usersRepository.remove(user)
            
            
            return "User deleted" + " " +user.email
        }
        throw new HttpException("No such user exist",400)
    }

    async deleteAllUser(){
        const users=await this.findAll()
        users.map((user)=>{
            this.deleteUserById(user.id)
        })

    }
   
    async updateUser(id:number,userDetails:any){
        const user=await this.findById(id)
        console.log(user,"in update");
        console.log(userDetails,id);
        
        if (user===undefined)
            throw new HttpException("No user exist",400)
        const {password,...rest}=userDetails
        
        
        const newUser={...user,...rest}
        await this.usersRepository.save(newUser)
        return "User Updated"
        
    }
    async addReserve(userId:number,bikeId:number){
        
        const user = await this.usersRepository.findOne({
            where: {id: userId },
            relations: ['bike'],
          })
        
        const bike =await this.bikeRepository.findOneBy({id:bikeId})
        
        if(user && bike)
        {
            const addBike=[...user.bike]
            addBike.push(bike)

            await this.usersRepository.save({...user,bike:[...addBike]})
            console.log(user,bike);
            
        }
        
    }

    async deleteReserve(userId:number,bikeId:number){
        const user = await this.usersRepository.findOne({
            where: {id: userId },
            relations: ['bike'],
          })
        
        const bike =await this.bikeRepository.findOneBy({id:bikeId})
        
        if(user && bike){
            const deleteBike= user.bike.filter((bike)=>{
                return bike.id!==bikeId
            })

            await this.usersRepository.save({...user,bike:[...deleteBike]})
            console.log(user,deleteBike);
            return "Reservation completed"
            
        }

        throw new HttpException("User/Bike not found",400)

    }

    async getBikeReserve(userId:number){

        const user = await this.usersRepository.findOne({
            where: {id: userId },
            relations: ['bike'],
          })
        
          if(user){
            return user.bike
          }

          throw new HttpException("User not found",400)

          
    }
}
