import { Controller,Get, Request,Post,Body, Delete, Param, Patch, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import ManagerGuard from '../auth/admin.guard';
import { UserService } from "./user.service";

@Controller('user')
export class UserController {
    constructor(private UserService:UserService) {
    }
    @UseGuards(AuthGuard('jwt'))
    @UseGuards(ManagerGuard)
    @Get()
    findAll(){
        return this.UserService.findAll()
    }
    
    @Post('/signup')
    signUp(@Body() body:any){
        return this.UserService.signUp(body)
    }
    
    @Post('/login')
    login(@Body() body:any){
        return this.UserService.login(body)
}
    @UseGuards(AuthGuard('jwt'))
    @Patch('/update/:id')
    updateUser(@Param('id') id:string,@Body() body){
        
        return this.UserService.updateUser(Number(id.slice(1)),body)
    }
    @UseGuards(AuthGuard('jwt'))
    @Delete('/delete/:id')
    deleteUserById(@Param('id') id:string ){
        return this.UserService.deleteUserById(Number(id.slice(1)))
    }
    @UseGuards(AuthGuard('jwt'))
    @Delete('deleteAll')
    deleteAll(){
        return this.UserService.deleteAllUser()
    }
    
    @Post('/addReserve')
    addReserve(@Body() body){
        return this.UserService.addReserve(body.userId,body.bikeId)
    }

    @Delete('/deleteReserve')
    deleteReserve(@Body() body){
        return this.UserService.deleteReserve(body.userId,body.bikeId)
    }

    @Get('/getBikeReserve/:id')
    getBikeReserve(@Param('id') id:string)
    {
        return this.UserService.getBikeReserve(Number(id.slice(1)))
    }
}
